﻿using AK.UI;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CharacterCollectionView : MonoBehaviour
{
    [Title("CollectionNodes")]
    public CharacterCollectionNodeView[] collectionNodes;

    [Title("CountBackwardsDisplay")]
    public GameObject countBackwardsDisplayObject;
    public TextMeshProUGUI CountBackwardsTime;
    public Image CountBackwardsMins;
    public Image CountBackwardsMin;
    public SpriteAnimPlayer couponCountAnimPlayer;

    [Title("Texts")]
    public TextMeshProUGUI currPage;
    public TextMeshProUGUI maxPage;

    [Title("Buttons")]
    public UIPointerEventHandler previousButton;
    public UIPointerEventHandler nextButton;
    public UIPointerEventHandler allClearButton;

    public ButtonEffectController previousButtonEffectController;
    public ButtonEffectController nextButtonEffectController;
    public ButtonEffectController allClearButtonEffectController;

    public Button allClearUIButton;

    //page 1 base
    public void DisplayCollection(List<CollectionPageDisplay> data, int page)
    {
        currPage.text = page.ToString();
        maxPage.text = data.Count.ToString();

        RemoveCapooCollectionView();
        for (int i = 0; i < collectionNodes.Length; i++)
        {
            if (i <= data[page - 1].pageCollections.Count - 1)
            {
                collectionNodes[i].Initial(data[page - 1].pageCollections[i]);
            }
        }
    }

    public void RemoveCapooCollectionView()
    {
        for(int i = 0; i < collectionNodes.Length; i++)
        {
            collectionNodes[i].ClearCollectionView();
        }
    }

    public void SetButtonEffect()
    {
        previousButton.OnPress += (eventData) => {
            if (eventData.button != PointerEventData.InputButton.Left) return;
            previousButtonEffectController.DoScale(new Vector3(-1.1f, 1.1f, 1.0f));
        };
        previousButton.OnRelease += (eventData) => {
            if (eventData.button != PointerEventData.InputButton.Left) return;
            previousButtonEffectController.DoScale(new Vector3(-1.0f, 1.0f, 1.0f));
        };

        nextButton.OnPress += (eventData) => {
            if (eventData.button != PointerEventData.InputButton.Left) return;
            nextButtonEffectController.DoScale(new Vector3(1.1f, 1.1f, 1.0f));
        };
        nextButton.OnRelease += (eventData) => {
            if (eventData.button != PointerEventData.InputButton.Left) return;
            nextButtonEffectController.DoScale(new Vector3(1.0f, 1.0f, 1.0f));
        };

        allClearButton.OnPress += (eventData) => {
            if (eventData.button != PointerEventData.InputButton.Left) return;
            if (!allClearUIButton.interactable) return;
            allClearButtonEffectController.DoScale(new Vector3(1.1f, 1.1f, 1.0f));
            SoundManager.Instance.SystemSound.PlayOneShot(SoundManager.Instance.sounds[(int)SoundRef.RecycleCapoo]);
        };
        allClearButton.OnRelease += (eventData) => {
            if (eventData.button != PointerEventData.InputButton.Left) return;
            if (!allClearUIButton.interactable) return;
            allClearButtonEffectController.DoScale(new Vector3(1.0f, 1.0f, 1.0f));
        };
    }

    public void DisplayCountBackwardsTime(int time)
    {
        CountBackwardsTime.text = time.ToString();

        CountBackwardsMins.gameObject.SetActive(time > 1);
        CountBackwardsMin.gameObject.SetActive(!(time > 1));
    }

    public void DisplayCountBackwardsTime(bool display)
    {
        countBackwardsDisplayObject.SetActive(display);
    }
}
