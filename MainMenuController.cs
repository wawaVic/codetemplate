﻿using DesktopMascotMaker;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainMenuController : MonoBehaviour
{
    public CharacterCameraController cameraController;
    public MainMenuView view;
    public CapooInput UIInput;
    public LoadingBlockController loadingBlockController;

    private int borderLimitOffsetWidth = 240;
    private int borderLimitOffsetHeight = 240;

    [Title("Collection")]
    public CharacterCollectionController collectionController;

    [Title("SystemSetting")]
    public SystemSettingController systemSettingController;

    private void Start()
    {
        //拖曳邊界極限值設定
        cameraController.SetBorderLimit(borderLimitOffsetWidth, borderLimitOffsetHeight, MainSystemController.Instance.localSystemSetting.extendedScreenSetting);
        //Initial Position
        cameraController.Left = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width / 2 - (int)(cameraController.MascotFormSize.x / 2.0f);
        cameraController.Top = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height / 2 - (int)(cameraController.MascotFormSize.y / 2.0f);


        UIInput.Init(UIInput.mascotmaker, UIInput.raycaster);

        //Buttons
        view.collectionTabButton.OnClick += (eventdata) => EnterCollection(eventdata);

        view.systemSettingTabButton.OnClick += (eventdata) => TabSwitch(4, eventdata);
        view.closeMenuButton.OnClick += (eventdata) => CloseMainMenu(eventdata);

        view.SetButtonEffect();

        //Debug
        //view.testButton1.OnClick += (eventdata) => RefreshCollectionData();
        view.resetButton.OnClick += (eventdata) => ResetCapooInfoData();
    }

    //Debug
    public void OnDebugMessageDisplay(string msg)
    {
        view.DebugMessageDisplay(msg);
    }

    #region UI Control
    //呼叫Collection API
    public void EnterCollection(PointerEventData pointerEventData)
    {
        if (pointerEventData.button != PointerEventData.InputButton.Left) return;

        loadingBlockController.DisplayLoadingBlock(true);
        StartCoroutine(MainSystemController.Instance.RefreshCollections((x) => {
            loadingBlockController.DisplayLoadingBlock(false);
            TabSwitch(1);
            collectionController.OnRefreshCollections(x);
        }));
    }

    //切換分頁
    public void TabSwitch(int tab, PointerEventData pointerEventData = null)
    {
        if (pointerEventData != null)
        {
            if (pointerEventData.button != PointerEventData.InputButton.Left) return;
        }

        view.collectionTab.SetActive(false);
        view.systemSettingTab.SetActive(false);

        switch (tab)
        {
            case 1:
                view.collectionTab.SetActive(true);
                break;
            case 4:
                view.systemSettingTab.SetActive(true);
                systemSettingController.DisplayHintConfirmView(false);
                systemSettingController.DisplayCreditView(false);
                break;
            default:
                break;
        }
    }

    private void CloseMainMenu(PointerEventData pointerEventData)
    {
        if (pointerEventData != null)
        {
            if (pointerEventData.button != PointerEventData.InputButton.Left) return;
        }

        //關閉時，清除圖鑑capoo Spine
        for (int i = 0; i < collectionController.view.collectionNodes.Length; i++)
        {
            collectionController.view.collectionNodes[i].ClearCollectionView();
        }
        MainSystemController.Instance.DisplayMainMenu(false);
    }

    public void OnEnterMainMenu()
    {
        view.DisplayMainMenuEffect();
    }

    public void DisplayMainMenu(bool display)
    {
        if (display)
        {
            view.mainObject.SetActive(true);
            cameraController.Show();
        }
        else
        {
            cameraController.Hide();
        }
    }

    //顯示上方剩餘coupon券數量
    public void OnDisplayCouponNumber(bool display)
    {
        view.DisplayCouponNumber(display);
    }
    #endregion

    #region Debug
    private void ResetCapooInfoData()
    {
        //清除
        //PlayerPrefs.DeleteAll();
    }
    #endregion
}
