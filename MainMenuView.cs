﻿using AK.UI;
using DesktopMascotMaker;
using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenuView : MonoBehaviour
{
    public CanvasGroup thisCanvasGroup;

    public GameObject mainObject;

    [Title("CouponNumber")]
    public SpriteAnimPlayer couponNumberAnimPlayer;

    [Title("Tab")]
    public GameObject collectionTab;
    public GameObject systemSettingTab;
    public UIPointerEventHandler collectionTabButton;
    public UIPointerEventHandler systemSettingTabButton;
    public UIPointerEventHandler closeMenuButton;

    public ButtonEffectController closeMenuButtonEffectController;

    public UIPointerEventHandler resetButton;
    public Text debugText;

    float enterEffectDuration = 0.5f;

    public void SetButtonEffect()
    {
        closeMenuButton.OnPress += (eventData) =>
        {
            if (eventData.button != PointerEventData.InputButton.Left) return;
            closeMenuButtonEffectController.DoScale(new Vector3(1.1f, 1.1f, 1.0f));
        };
        closeMenuButton.OnRelease += (eventData) =>
        {
            if (eventData.button != PointerEventData.InputButton.Left) return;
            closeMenuButtonEffectController.DoScale(new Vector3(1.0f, 1.0f, 1.0f));
        };
    }

    public void DisplayMainMenuEffect()
    {
        //先不顯示capoo圖鑑
        collectionTab.SetActive(false);
        StartCoroutine(DelayDisplayCollectionTabView(enterEffectDuration));

        transform.DOPunchScale(new Vector3(0.3f, 0.3f, 1.0f), enterEffectDuration, 4, 1);
    }

    IEnumerator DelayDisplayCollectionTabView(float time)
    {
        yield return new WaitForSeconds(time);
        collectionTab.SetActive(true);
    }

    public void DebugMessageDisplay(string msg)
    {
        debugText.text = msg;
    }

    public void DisplayCouponNumber(bool display)
    {
        if (display)
        {
            couponNumberAnimPlayer.gameObject.SetActive(true);
            couponNumberAnimPlayer.Initial(0);
            StartCoroutine(couponNumberAnimPlayer.Play());
        }
        else
        {
            couponNumberAnimPlayer.gameObject.SetActive(false);
        }
    }
}
