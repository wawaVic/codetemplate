﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// 圖鑑Controller
/// </summary>
public class CharacterCollectionController : MonoBehaviour
{
    public CharacterCollectionView view;

    private List<CollectionPageDisplay> currPageDatas;

    public List<CharacterCollection> currCapooList;
    private Dictionary<string, CharacterCollection> capooCollectionDic = new Dictionary<string, CharacterCollection>(); //快速查找用

    private void Start()
    {
        SetAllClearUIButton();

        view.previousButton.OnClick += (eventData) => OnPageModify(eventData, -1);
        view.nextButton.OnClick += (eventData) => OnPageModify(eventData, 1);
        view.allClearButton.OnClick += (eventData) =>
        {
            if (eventData.button != PointerEventData.InputButton.Left) return;
            GenerateCapooController.Instance.OnClearCapoo();
            ResetEachCharacterToUnSummoned();
        };

        view.SetButtonEffect();
    }

    //進入圖鑑、更新圖鑑、翻到首頁
    public void OnRefreshCollections(List<CharacterCollection> list)
    {
        OnDisplayCurrPageCollections(list);
        //碰撞合成倒數小動畫
        view.couponCountAnimPlayer.Initial(0);
        StartCoroutine(view.couponCountAnimPlayer.Play());
    }

    //翻到第1頁或當前頁面
    public void OnDisplayCurrPageCollections(List<CharacterCollection> data)
    {
        SetCurrPageDatas(data);

        if (int.TryParse(view.currPage.text, out int currPage))
        {
            view.DisplayCollection(currPageDatas, currPage);
            PageModifyButtonHide(currPage);
        }
        else
        {
            view.DisplayCollection(currPageDatas, 1);
            PageModifyButtonHide(1);
        }
    }

    private void SetCurrPageDatas(List<CharacterCollection> data)
    {
        currPageDatas = new List<CollectionPageDisplay>();

        int maxPerPage = view.collectionNodes.Length;

        int j = 0;
        List<CharacterCollection> tempPage = new List<CharacterCollection>();
        for (int i = 0; i < data.Count; i++)
        {
            tempPage.Add(data[i]);
            j++;
            //滿頁或是最後一筆資料
            if (j == maxPerPage || (i + 1) == data.Count)
            {
                currPageDatas.Add(new CollectionPageDisplay()
                {
                    pageCollections = tempPage
                });
                tempPage = new List<CharacterCollection>();
                j = 0;
            }
        }
    }

    //換頁
    public void OnPageModify(PointerEventData pointerEventData, int value)
    {
        if (pointerEventData != null)
        {
            if (pointerEventData.button != PointerEventData.InputButton.Left) return;
        }

        if (int.TryParse(view.currPage.text, out int currPage))
        {
            int targetPage = (currPage + value);
            //minimum,max
            if (targetPage < 1 || targetPage > currPageDatas.Count) return;

            view.DisplayCollection(currPageDatas, targetPage);

            //設定換頁按鈕顯示或隱藏
            PageModifyButtonHide(targetPage);
        }
    }

    //設定換頁按鈕顯示或隱藏
    private void PageModifyButtonHide(int page)
    {
        //總頁數只有1頁
        if (currPageDatas.Count <= 1)
        {
            view.previousButton.gameObject.SetActive(false);
            view.nextButton.gameObject.SetActive(false);
            return;
        }

        if (page.Equals(1))
        {
            //第1頁
            view.previousButton.gameObject.SetActive(false);
            view.nextButton.gameObject.SetActive(true);

        }
        else if (page.Equals(currPageDatas.Count))
        {
            //最後1頁
            view.previousButton.gameObject.SetActive(true);
            view.nextButton.gameObject.SetActive(false);

        }
        else
        {
            view.previousButton.gameObject.SetActive(true);
            view.nextButton.gameObject.SetActive(true);
        }
    }

    public void SetAllClearUIButton()
    {
        int currCapooCount = GenerateCapooController.Instance.capooParent.childCount;
        if (currCapooCount > 0)
        {
            view.allClearUIButton.interactable = true;
        }
        else
        {
            view.allClearUIButton.interactable = false;
        }
    }

    public void SetEachCharacterClearUIButton(string collectID)
    {
        GenerateCapooController.Instance.OnClearCapoo(collectID);
    }

    public void ResetEachCharacterToUnSummoned()
    {
        foreach (CharacterCollectionNodeView node in view.collectionNodes)
        {
            node.ResetToUnSummoned();
        }
    }

    public void HideAllBugcatsName()
    {
        foreach (CharacterCollectionNodeView node in view.collectionNodes)
        {
            node.HideBugcatName();
        }
    }

    #region IEnumerator
    //可取得碰撞券倒數計時
    public IEnumerator CountBackwards(int countBackwardTime)
    {
        MainSystemController.Instance.isGameTimeDisplayCounting = true;
        //當剩餘次數已達上限時，不顯示倒數
        //已無剩餘Capoo可碰撞時，不顯示倒數
        if (WebApiManager.Instance.appInfo.appUserInfo.gacha_count >= MainSystemController.Instance.gameParameters.maxGachaCount
            || !MainSystemController.Instance.IsCharacterCollectionsUnlock)
        {
            view.DisplayCountBackwardsTime(false);
            MainSystemController.Instance.isGameTimeDisplayCounting = false;
            yield break;
        }

        view.DisplayCountBackwardsTime(true);
        view.DisplayCountBackwardsTime(countBackwardTime);

        while (countBackwardTime >= 1)
        {
            yield return new WaitForSeconds(60.0f);

            countBackwardTime -= 1;
            view.DisplayCountBackwardsTime(countBackwardTime);

            if (countBackwardTime <= 0)
            {
                MainSystemController.Instance.isGameTimeDisplayCounting = false;
                yield break;
            }
        }
    }
    #endregion
}

public class CollectionPageDisplay
{
    public List<CharacterCollection> pageCollections;
}
