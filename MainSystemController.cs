﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AK.Table;
using DesktopMascotMaker;
using Sirenix.OdinInspector;


public enum CapooActionNameKey
{
    idle_idle,
    idle_full,
    walk,
    run,
    wild,
    roll,
    air,
    fall,
    hit,
    pet,
    prone,
    catchUp,
    stackFog,
    stackDown,
    foodAppear,
    chew,
    enjoy,
    pooing,
    waitCleanPoo,
    pooRemoving,
    pooRemoveAfter,
    dizzy,
    summonAppear,
    recycle,
    merge
}

public class MainSystemController : MonoBehaviour
{
    [Title("UI")]
    //CameraController
    public CharacterCameraController mainMenuEntranceCameraController;
    public CharacterCameraController characterReceiveHintCameraController;

    //Controller
    public MainMenuEntranceController mainMenuEntranceController;
    public MainMenuController mainMenuController;
    public CharacterCollectionController collectionController;
    public CharacterReceiveHintController characterReceiveHintController;
    public ErrorHandleHintController errorHandleHintController;

    public static MainSystemController Instance;

    private void Awake()
    {
        if (Instance == null) Instance = this;

        //讀取設定資料
        localSystemSetting = LocalDataRecord.RefreshLocalSystemSetting();
    }

    //Collection
    //capoo總表
    public List<CapooCode> capooCompletedList;

    //Desk Info
    public LocalSystemSetting localSystemSetting { get; set; }

    //GameParameters
    private GameParameter _gameParameters;
    public GameParameter gameParameters { get { return _gameParameters; } }
    public int currStayActionCapooNumber { get; set; }
    public bool IsCharacterCollectionsUnlock { get; set; } //是否還有未解鎖的角色: true => 還有未解鎖的角色, false => 所有角色都已解鎖
    public bool IsCapooGaining { get; set; }
    public List<CharacterPreCollection> characterPreCollections = new List<CharacterPreCollection>();

    //遊戲時間記數
    public bool isGameTimeCounting { get; set; }
    //倒數計時顯示
    public bool isGameTimeDisplayCounting { get; set; }

    public Dictionary<CapooActionNameKey, List<string>> cpooActions = new Dictionary<CapooActionNameKey, List<string>>();

    public bool debugMode = false;
    public System.DateTime currTime { get; set; }
    private string upgradeSetUnlockCapoos { get; set; }

    private void Start()
    {
        mainMenuEntranceController.DisplayMainEntranceMenu(false);
        Initial();
    }

    private void Initial()
    {
        currStayActionCapooNumber = 0;
        IsCapooGaining = false;
        isGameTimeCounting = false;
        currTime = System.DateTime.UtcNow.AddHours(8); //決定更新包顯示到哪些貓蟲

        //讀表
        LoadTableByStep();
    }

    private void Update()
    {
#if USE_DEV_SERVER
        //Demo清除資料、增加券
        if (Input.GetKey(KeyCode.Z))
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                //清除
                PlayerPrefs.DeleteAll();
                Application.Quit();
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                debugMode = !debugMode;
            }
        }
#endif
    }

    private IEnumerator InitialCoroutine()
    {
        isGameTimeDisplayCounting = false;

        //刷新圖鑑，並獲得當前解鎖狀態
        yield return RefreshCollections(CharacterCollectionsUnlockVerify);

        //Refresh app user Info
        yield return RefreshAppUserInfo(RefreshCouponNumber);

        //可取得碰撞券倒數計時
        if (!isGameTimeDisplayCounting) StartCoroutine(collectionController.CountBackwards(GetCouponCountBackwardsTime()));

        //關閉顯示MainMenu
        mainMenuController.DisplayMainMenu(false);

        //顯示MainMenuEntrance
        mainMenuEntranceController.DisplayMainEntranceMenu(true);

        //紀錄遊戲時間啟動     
        OnCountGameTime();

        //紀錄成就啟動
        AchievementRecord.Instance.Initial();

        //固定時間檢查Token是否有效
        OnTokenVerify();
    }

    //依順序讀表
    int LoadTableStep = 1;
    private void LoadTableByStep()
    {
#if USE_DEV_SERVER
        string table_path = TableManager.Instance.appsettingsTable.Dev[0].TablePath;
        string table_version = TableManager.Instance.appsettingsTable.Dev[0].TableVer;
#elif USE_PUBLISH_SERVER
        string table_path = TableManager.Instance.appsettingsTable.Public[0].TablePath;
        string table_version = TableManager.Instance.appsettingsTable.Public[0].TableVer;
#endif

        switch (LoadTableStep)
        {
            case 1:
                StartCoroutine(TableManager.Instance._LoadData(table_path, table_version, "Game_Parameter", TableManager.TableName.GameParameter, LoadTableFail, () =>
                {
                    LoadTableStep++;
                    Debug.Log("Game_Parameter File Success Load");
                    LoadTableByStep();
                }));
                break;
            case 2:
                StartCoroutine(TableManager.Instance._LoadData(table_path, table_version, "Capoo_DefaultSet", TableManager.TableName.CapooDefaultSet, LoadTableFail, () =>
                {
                    LoadTableStep++;
                    Debug.Log("Capoo_DefaultSet File Success Load");
                    LoadTableByStep();
                }));
                break;
            case 3:
                StartCoroutine(TableManager.Instance._LoadData(table_path, table_version, "Capoo_BasicSet", TableManager.TableName.CapooBasicSet, LoadTableFail, () =>
                {
                    LoadTableStep++;
                    Debug.Log("Capoo_BasicSet File Success Load");
                    LoadTableByStep();
                }));
                break;
            case 4:
                StartCoroutine(TableManager.Instance._LoadData(table_path, table_version, "Capoo_UpgradeSet", TableManager.TableName.CapooUpgradeSet, LoadTableFail, () =>
                {
                    LoadTableStep++;
                    Debug.Log("Capoo_UpgradeSet File Success Load");
                    LoadTableByStep();
                }));
                break;
            case 5:
                StartCoroutine(TableManager.Instance._LoadData(table_path, table_version, "Game_Time", TableManager.TableName.GameTime, LoadTableFail, () =>
                {
                    LoadTableStep++;
                    Debug.Log("Game_Time File Success Load");
                    LoadTableByStep();
                }));
                break;
            case 6:
                StartCoroutine(TableManager.Instance._LoadData(table_path, table_version, "Capoo_Info", TableManager.TableName.CapooInfo, LoadTableFail, () =>
                {
                    LoadTableStep++;
                    Debug.Log("Capoo_Info File Success Load");
                    LoadTableByStep();
                }));
                break;
            case 7:
                StartCoroutine(TableManager.Instance._LoadData(table_path, table_version, "Capoo_Action", TableManager.TableName.CapooAction, LoadTableFail, () =>
                {
                    LoadTableStep++;
                    Debug.Log("Capoo_Action File Success Load");
                    LoadTableByStep();
                }));
                break;
            case 8:
                StartCoroutine(TableManager.Instance._LoadData(table_path, table_version, "Game_Achievement", TableManager.TableName.AchievementNames, LoadTableFail, () =>
                {
                    LoadTableStep++;
                    Debug.Log("Game_Achievement File Success Load");
                    LoadTableByStep();
                }));
                break;
            case 9:
                StartCoroutine(TableManager.Instance._LoadData(table_path, table_version, "Capoo_Name", TableManager.TableName.CapooName, LoadTableFail, () =>
                {
                    LoadTableStep++;
                    Debug.Log("Capoo_Name File Success Load");
                    LoadTableByStep();
                }));
                break;
            default:
                LoadTableComplete();
                return;
        }
    }

    //讀表失敗
    private void LoadTableFail()
    {
        errorHandleHintController.Initial(OperateHint.NetworkAbnormalLocalHint_tw + "\r\n" + OperateHint.NetworkAbnormalLocalHint_en
            , () =>
            {
                errorHandleHintController.cameraController.Hide();
                LoadTableByStep();
            });
        errorHandleHintController.cameraController.Show();
    }

    //讀表完成
    private void LoadTableComplete()
    {
        //組成當前完整角色列表
        SetCompletedList();
        upgradeSetUnlockCapoos = GetUpgradeSetUnlockCapoos();

        //GameParameters
        _gameParameters = new GameParameter();
        _gameParameters.idleTimeMin = TableManager.Instance.gameparameterTable[0].idleTimeMin;
        _gameParameters.idleTimeMax = TableManager.Instance.gameparameterTable[0].idleTimeMax;
        _gameParameters.hungryProbability = TableManager.Instance.gameparameterTable[0].hungryProbability;
        _gameParameters.pooProbability = TableManager.Instance.gameparameterTable[0].pooProbability;
        _gameParameters.idleProbability = TableManager.Instance.gameparameterTable[0].idleProbability;
        _gameParameters.walkProbability = TableManager.Instance.gameparameterTable[0].walkProbability;
        _gameParameters.runProbability = TableManager.Instance.gameparameterTable[0].runProbability;
        _gameParameters.wildProbability = TableManager.Instance.gameparameterTable[0].wildProbability;
        _gameParameters.walkSpeed = TableManager.Instance.gameparameterTable[0].walkSpeed;
        _gameParameters.runSpeed = TableManager.Instance.gameparameterTable[0].runSpeed;
        _gameParameters.wildSpeed = TableManager.Instance.gameparameterTable[0].wildSpeed;
        _gameParameters.walkRadius = TableManager.Instance.gameparameterTable[0].walkRadius;
        _gameParameters.runRadius = TableManager.Instance.gameparameterTable[0].runRadius;
        _gameParameters.wildRadius = TableManager.Instance.gameparameterTable[0].wildRadius;
        _gameParameters.stayActionNumberMax = TableManager.Instance.gameparameterTable[0].stayActionNumberMax;
        _gameParameters.stayActionDuration = TableManager.Instance.gameparameterTable[0].stayActionDuration;
        _gameParameters.displayCharacterMaxNumber = TableManager.Instance.gameparameterTable[0].displayCharacterMaxNumber;
        _gameParameters.countGameTimeInterval = TableManager.Instance.gameparameterTable[0].countGameTimeInterval;
        _gameParameters.maxGachaCount = TableManager.Instance.gameparameterTable[0].maxGachaCount;

        //組合貓蟲動作動畫
        foreach (CapooActionNameKey actionNameKey in (CapooActionNameKey[])System.Enum.GetValues(typeof(CapooActionNameKey)))
        {
            cpooActions.Add(actionNameKey, CapooActionHelper.GetByKey(actionNameKey.ToString()));
        }

        StartCoroutine(InitialCoroutine());
    }

    #region Coupon

    //顯示券數量
    private void RefreshCouponNumber(CapooAppUserInfo data)
    {
        if (data.gacha_count > 0 && IsCharacterCollectionsUnlock)
        {
            mainMenuEntranceController.DisplayCouponNumber(true);
            mainMenuController.OnDisplayCouponNumber(true);
        }
        else
        {
            mainMenuEntranceController.DisplayCouponNumber(false);
            mainMenuController.OnDisplayCouponNumber(false);
        }

        //全部解鎖，關閉倒數
        if (!IsCharacterCollectionsUnlock)
        {
            collectionController.view.DisplayCountBackwardsTime(false);
        }
    }

    //下一張券剩餘時間
    public int GetCouponCountBackwardsTime()
    {
        //下一張券所需時間
        int.TryParse(GameTimeHelper.GetByID(WebApiManager.Instance.appInfo.appUserInfo.lastAccumulateID.ToString()).GameTime
            , out int nextCouponAccumulateOriginal);
        //上次增加的時間 + 下次增加count需要的累計 - 目前已累計
        int nextCouponAccumulate = WebApiManager.Instance.appInfo.appUserInfo.last_incr_gacha + nextCouponAccumulateOriginal - WebApiManager.Instance.appInfo.appUserInfo.accumulateAttrCount;

        return nextCouponAccumulate;
    }

    //有沒有未解鎖的角色
    public void CharacterCollectionsUnlockVerify(List<CharacterCollection> data)
    {
        //找現在的列表中有沒有，已購買 +* 未解鎖的角色
        IsCharacterCollectionsUnlock = false;

        foreach (CharacterCollection character in data)
        {
            if (!character.isLocked && !character.acquired)
            {
                IsCharacterCollectionsUnlock = true;
                break;
            }
        }
    }

    //有沒有未解鎖的角色
    public void CharacterCollectionsUnlockVerify(bool isUnlock)
    {
        IsCharacterCollectionsUnlock = isUnlock;
    }

    //定時呼叫API紀錄遊戲時間
    private void OnCountGameTime()
    {
        if (!isGameTimeCounting) StartCoroutine(DoCountGameTime());
    }

    IEnumerator DoCountGameTime()
    {
        isGameTimeCounting = true;
        //券已滿或已無可解鎖角色時，不再記數遊戲時間
        if (WebApiManager.Instance.appInfo.appUserInfo.gacha_count >= gameParameters.maxGachaCount
            || !IsCharacterCollectionsUnlock)
        {
            isGameTimeCounting = false;
            yield break;
        }

        System.TimeSpan t = System.TimeSpan.FromMinutes(gameParameters.countGameTimeInterval);
        yield return new WaitForSeconds((float)t.TotalSeconds);

        //呼叫API紀錄時間
        var request = WebApiManager.Instance.CountGameTime();
        yield return request;
        var response = request.GetResult<NormalResponse<string>>();

        if (response)
        {
            if (response.data.success)
            {
                Debug.Log("CountGameTime Done! " + response.data.message);
            }
            else
            {
                Debug.Log("CountGameTime Fail! " + response.data.message);
            }
        }

        //延遲5秒，避免server冷卻時間檔到
        yield return new WaitForSeconds(5.0f);
        yield return RefreshAppUserInfo(RefreshCouponNumber);

        isGameTimeCounting = false;

        //當剩餘次數已達上限時，不再紀錄遊戲時間
        if (WebApiManager.Instance.appInfo.appUserInfo.gacha_count < gameParameters.maxGachaCount)
        {
            OnCountGameTime();
        }
    }

    //定時檢查Token
    public void OnTokenVerify()
    {
        StartCoroutine(DoTokenVerify());
        //若大書開啟、刷新圖鑑資料
        //防弊，更新大書圖鑑資料
        if (mainMenuController.cameraController.IsRender() && mainMenuController.collectionController.view.isActiveAndEnabled)
        {
            mainMenuController.loadingBlockController.DisplayLoadingBlock(true);
            StartCoroutine(RefreshCollections((x) =>
            {
                collectionController.OnDisplayCurrPageCollections(x);
                mainMenuController.loadingBlockController.DisplayLoadingBlock(false);
            }));
        }
    }

    //Token Verify
    IEnumerator DoTokenVerify()
    {
        System.TimeSpan t = System.TimeSpan.FromMinutes(gameParameters.countGameTimeInterval);
        yield return new WaitForSeconds((float)t.TotalSeconds);

        var request = WebApiManager.Instance.VerifyToken();
        yield return request;
        var response = request.GetResult<LoginInfo>();
        yield return response;

        if (response)
        {
            OnTokenVerify();
        }
    }
    #endregion

    #region Show main menu
    public void DisplayMainMenu(bool display)
    {
        if (display)
        {
            mainMenuEntranceController.DisplayMainEntranceMenu(false);
            mainMenuController.DisplayMainMenu(true);
            //打開main menu UI動態
            mainMenuController.OnEnterMainMenu();
            //預設翻回到圖鑑頁面
            mainMenuController.TabSwitch(1);
            SoundManager.Instance.SystemSound.PlayOneShot(SoundManager.Instance.sounds[(int)SoundRef.OpenBook]);
        }
        else
        {
            mainMenuEntranceController.DisplayMainEntranceMenu(true);
            mainMenuController.DisplayMainMenu(false);
        }
    }
    #endregion

    //刷新AppUserInfo
    public IEnumerator RefreshAppUserInfo(UnityEngine.Events.UnityAction<CapooAppUserInfo> action = null)
    {
        var request = WebApiManager.Instance.RefreshAppUserInfo();
        yield return request;
        var response = request.GetResult<CapooAppUserInfo>();

        if (response)
        {
            Debug.Log("RefreshAppUserInfo Done!");
            action?.Invoke(response.data);

            //可取得碰撞券倒數計時
            if (!isGameTimeDisplayCounting)
            {
                StartCoroutine(collectionController.CountBackwards(GetCouponCountBackwardsTime()));
            }
        }
        else
        {
            Debug.Log("RefreshAppUserInfo Fail!");
        }
    }

    #region 圖鑑
    //建立完整總表
    private void SetCompletedList()
    {
        capooCompletedList = new List<CapooCode>();
        foreach (CapooInfo item in TableManager.Instance.capooinfoTable)
        {
            capooCompletedList.Add(new CapooCode()
            {
                capoo_code = item.id
            });
        }
    }

    private string GetUpgradeSetUnlockCapoos()
    {
        string result = "";
        List<string> activeProductCodes = CapooUpgradeSetHelper.GetByStartTime(currTime);
        foreach (string productCode in activeProductCodes)
        {
            CapooUpgradeSet tempCapooUpgradeSet = CapooUpgradeSetHelper.GetByID(productCode);
            result += tempCapooUpgradeSet.unlockCapoo;
        }

        return result;
    }
    #endregion

    #region 獲得Capoo
    //取得capoo，加至PreCollection
    public void JoinCapooToPreCollection(string capooID)
    {
        //PreCollectionList已存在、不加入
        foreach (CharacterPreCollection preCollection in characterPreCollections)
        {
            if (preCollection.collectID.Equals(capooID)) return;
        }

        CharacterPreCollection temp = new CharacterPreCollection()
        {
            collectID = capooID,
            cdt = System.DateTime.Now
        };
        characterPreCollections.Add(temp);
    }

    //顯示領取畫面
    public IEnumerator DisplayReceiveHint()
    {
        yield return new WaitForSeconds(0.1f);

        //是否正在顯示
        if (!characterReceiveHintCameraController.IsRender())
        {
            //看有沒有待領取
            List<CharacterPreCollection> temp = characterPreCollections;
            if (temp != null)
            {
                if (temp.Count > 0)
                {
                    characterReceiveHintController.Initial(temp[0]);
                    characterReceiveHintCameraController.Show();
                    SoundManager.Instance.SystemSound.PlayOneShot(SoundManager.Instance.sounds[(int)SoundRef.TakePhoto]);
                    //關閉MainMenu
                    DisplayMainMenu(false);
                    //MainMenuEntrance關閉互動
                    mainMenuEntranceController.SetMainButtonInteractable(false);
                }
            }
        }
        else
        {
            yield break;
        }
    }

    [Button]
    public void TestHint(string id)
    {
        CharacterPreCollection data = new CharacterPreCollection
        {
            collectID = id,
            cdt = System.DateTime.Now
        };
        List<CharacterPreCollection> temp = new List<CharacterPreCollection>();
        temp.Add(data);
        characterReceiveHintController.Initial(temp[0]);
        characterReceiveHintCameraController.Show();
        SoundManager.Instance.SystemSound.PlayOneShot(SoundManager.Instance.sounds[(int)SoundRef.TakePhoto]);
        //關閉MainMenu
        DisplayMainMenu(false);
        //MainMenuEntrance關閉互動
        mainMenuEntranceController.SetMainButtonInteractable(false);
    }

    //獲得新capoo
    public IEnumerator GainNewCapoo(UnityEngine.Events.UnityAction action = null)
    {
        IsCapooGaining = true;

        var request = WebApiManager.Instance.GainNewCapoo();
        yield return request;
        var response = request.GetResult<NormalResponse<string>>();

        if (response)
        {
            if (response.data.success)
            {
                //碰撞成功後的閃光效果
                action?.Invoke();

                string newCapooID = response.data.data;
                //將新取得的capoo ID 加入characterPreCollections
                JoinCapooToPreCollection(newCapooID);

                //將新取得的capoo ID 加入本地New資料
                LocalDataRecord.AddNewCapooRecord(newCapooID);

                //將新取得的capoo顯示在領取視窗
                yield return DisplayReceiveHint();

                //確認目前是不是已經解鎖全部角色
                yield return CollectionsUnlockVerify(CharacterCollectionsUnlockVerify);

                //更新剩餘的碰撞券
                yield return new WaitForSeconds(2.0f);
                yield return RefreshAppUserInfo(RefreshCouponNumber);

                //碰撞成功後，再呼叫記數遊戲時間
                OnCountGameTime();
            }
            IsCapooGaining = false;
        }
        else
        {
            IsCapooGaining = false;
        }
    }
    #endregion

    #region Collection IEnumerator
    //更新圖鑑
    public IEnumerator RefreshCollections(System.Action<List<CharacterCollection>> function)
    {
        var request = WebApiManager.Instance.RefreshCapooCollection();
        yield return request;
        var response = request.GetResult<List<CharacterCollection>>();
        yield return response;

        if (response)
        {
            Dictionary<string, CharacterCollection> tempCapooCollectionDic = new Dictionary<string, CharacterCollection>();
            for (int i = 0; i < response.data.Count; i++)
            {
                tempCapooCollectionDic.Add(response.data[i].collect_id, response.data[i]);
            }

            List<string> tempNewCapoos = LocalDataRecord.GetNewCapooRecord().newCapoos;

            //跟總表比對
            //已解鎖(可召喚) + 未解鎖(黑影)
            List<CharacterCollection> tempAcquiredCapooList = new List<CharacterCollection>();
            //未取得(問號)
            List<CharacterCollection> tempLockedCapooList = new List<CharacterCollection>();

            //從總表一個個檢查，看哪些要放入圖鑑顯示
            foreach (AK.Table.CapooCode capooCode in MainSystemController.Instance.capooCompletedList)
            {
                CharacterCollection tempCharacterCollection = new CharacterCollection()
                {
                    collect_id = capooCode.capoo_code,
                    acquired = false,
                    isLocked = true
                };

                //看是不是新取得的角色
                if (tempNewCapoos != null)
                {
                    if (tempNewCapoos.Count > 0)
                    {
                        if (tempNewCapoos.Contains(tempCharacterCollection.collect_id)) tempCharacterCollection.isNewGain = true;
                    }
                }

                if (tempCapooCollectionDic.TryGetValue(tempCharacterCollection.collect_id, out CharacterCollection collection))
                {
                    tempCharacterCollection.account_id = collection.account_id;
                    tempCharacterCollection.acquired = collection.acquired;
                    tempCharacterCollection.isLocked = false;
                    if (collection.acquired) tempCharacterCollection.cdt = collection.cdt;

                    tempAcquiredCapooList.Add(tempCharacterCollection);
                }
                else
                {
#if USE_PUBLISH_SERVER
                    //UpgradeSet中的開放時間已到，才可顯示未取得(問號)
                    if (upgradeSetUnlockCapoos.Contains(tempCharacterCollection.collect_id))
                    {
                        tempLockedCapooList.Add(tempCharacterCollection);
                    }
#elif USE_DEV_SERVER
                    tempLockedCapooList.Add(tempCharacterCollection);
#endif
                }
            }

            //排序
            /*
             * 第一區塊：已解鎖(可召喚) + 未解鎖(黑影)
             * 第二區塊：未取得(問號)
             * 第一區塊之後接續第二區塊
             * 各區塊的貓蟲順序依照ID小到大
             */
            List<CharacterCollection> tempCurrCapooList = new List<CharacterCollection>();
            tempCurrCapooList.AddRange(tempAcquiredCapooList);
            tempCurrCapooList.AddRange(tempLockedCapooList);

            function?.Invoke(tempCurrCapooList);
        }
    }

    //確認圖鑑有無未解鎖的角色
    public IEnumerator CollectionsUnlockVerify(System.Action<bool> function = null)
    {
        var request = WebApiManager.Instance.RefreshCapooCollection();
        yield return request;
        var response = request.GetResult<List<CharacterCollection>>();
        yield return response;

        if (response)
        {
            for (int i = 0; i < response.data.Count; i++)
            {
                if (!response.data[i].acquired)
                {
                    function?.Invoke(true);
                    yield break;
                }
            }
            function?.Invoke(false);
        }
    }
    #endregion
}